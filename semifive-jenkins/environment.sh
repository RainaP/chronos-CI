#!/bin/bash
unset MODULEPATH

# Source SemiFive Tools
if [[ -f /semifive/tools/Modules/default/init/zsh ]]; then
	     source /semifive/tools/Modules/default/init/zsh
     fi
     if [[ -f /semifive/tools/Modules/default/init/bash_completion ]]; then
	          bash /semifive/tools/Modules/default/init/bash_completion
	  fi

module avail
module load sifive/wake/0.19.2
module load riscv-tools/2019.08.0
module load semifivesnippet/module
module load python/python/3.7.1 
