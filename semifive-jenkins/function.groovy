def configuration() {
    env.project                  = "federation"
    env.wakeVersion              = "0.19.2"
    env.gitlab                   = "portal"
    env.group                    = "chronos"

    source_branch = "$gitlabSourceBranch"
    target_branch = "$gitlabTargetBranch"    
}

def path(){
    tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
    tmp_basedir = sh(script: "basename ${tmp_pwd}", returnStdout:true).trim()
    ciPath = "continuous-integration/semifive-jenkins"
    archivePath = "/user/jenkins/archive"
    result_path = "${tmp_pwd}/${env.project}/build/soc-sesame-sifive/api-soctbgen-sifive/wake/sesameDUT/sim/vcs/execute" 
}

def getGitBranchName(){
    return sh(returnStdout: true, script: """echo "${scm.branches[0].name}" | awk -F "/" '{print \$2}'""").trim()
}

def checkBranch(){
    default_branch = function.getGitBranchName()
    println(default_branch)    

    if(target_branch != default_branch){
        println("CI skipped")
        sh "exit 0"
        
    }
}

def prepareGit(){
    println("CI applied to ${target_branch}")
    sh """ 
        git clone git@${env.gitlab}:${env.group}/${env.project}.git -b ${target_branch}
        cd ${env.project}
        git merge origin/${target_branch} origin/${source_branch}                               
        ./scripts/quick-submodule-update-no-aes
    """
}

def runSim(){
    println("CI applied to ${target_branch}")
    timeout(time: 420, unit: 'MINUTES'){                      
        tee("full.log"){                            
            sh """
                cd ${env.project}
                source ${ciPath}/environment.sh
                wake --init .
                source ${ciPath}/pre-mr-ci.sh
            """
        }
    }   
}

def archive(){
    try{    
        if(fileExists("${archivePath}/${JOB_NAME}") == false){
            sh "mkdir -p ${archivePath}/${JOB_NAME}"
        }
        sh "tar -czf ${archivePath}/${JOB_NAME}/${JOB_NAME}.${BUILD_NUMBER}.tar.gz --ignore-failed-read --exclude=.fuse ../${tmp_basedir}"
    }catch(e){
        println("archive error")
    }
}

def checkFailure(){   
    dir("${result_path}"){
        sh """
            ls > test_list.log                            
            sed -i '/@tmp/d' test_list.log
            sed -i '/.log/d' test_list.log
        """
        file = readFile 'test_list.log'
        test = file.split('\n')
        list = []

        for(each in test){
            list.add(each as String)
        }
        
        for(i = 0; i < list.size(); i++){
            println("${list[i]}")
            dir("${result_path}/${list[i]}") {
                read = readFile 'status.log'
                if((read =~ /PASSED/)){
                    sh "exit 0"
                }else{
                    sh "exit 1"
                }
            }                           
        }
    }  
}
return this
